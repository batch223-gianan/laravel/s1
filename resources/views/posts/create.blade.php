@extends('layouts.app')

@section('content')
    <form method="POST" action="/posts">
        {{-- Cross-Site Request Forgery --}}
        @csrf
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title">
            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" id="content" cols="30" rows="3" class="form-control"></textarea>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary">Create Post</button>
                </div>
            </div>
        </div>
    </form>
@endsection