@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <img src="https://laravelnews.imgix.net/images/laravel-featured.png" alt="laravel-logo" class="w-50">
        <h1 style="text-align: center" class="mt-5">Featured Posts</h1>
    </div>
</div>

<div class="d-flex flex-column align-items-center">
    @foreach($posts as $post)
    <div class="card text-center mb-1 w-50">
        <div class="card-body">
            <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
            <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
            {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
        </div>
    </div>
    @endforeach
</div>

@endsection
