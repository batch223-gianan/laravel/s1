<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a from for a blog post creation.

    public function create(){
        return view('posts.create');
    }
    //Action to reeive form data and subsequently store said data in the posts table.

    public function store(Request $request){
        if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    //action that will return a view showing all blogposts.
    public function index(){
        $posts = Post::all()->where('isActive', true);
        return view('posts.index')->with('posts', $posts);
    }

        //Activity for s02
    //action that will return a view showing random blog posts
    public function welcome()
    {   
        //$posts = Post::all();
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->get();
        return view('welcome')->with('posts',$posts);
    }

    //action for showing only the post authored by the authenticated user
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    //action that will return a view showing a speicifc post using the URL parameter $id to query for hte database entry to be show
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post',$post);
    }

    public function edit($id)
    {
        // $posts = Post::all();
        if(Auth::user()){
            $post = Post::find($id);
            return view('posts.edit')->with('post',$post);
        } else {
            return redirect('/login');
        }
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if(Auth::user() -> id === $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }
    
    public function destroy($id)
    {   
        $post = Post::find($id);
        if(Auth::user()->id===$post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }

    public function archive($id) {
        $post = Post::find($id);

        // if authenticated users id is the same as the posts user-id
        if(Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();

            return redirect('/posts');
        }
        
    }

    public function unarchive($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
            $post->save();
        }
        return redirect('/myPosts');
    }



}
